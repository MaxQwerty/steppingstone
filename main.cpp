#include "initialwidget.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    InitialWidget w;
    w.show();

    return a.exec();
}
