#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>
#include <QtWidgets>

struct Coordinate
{
    int i, j;
    QList<Coordinate *> nextCoordinates;
    Coordinate(int I, int J) : i(I), j(J)
    {
    }
    bool operator ==(const Coordinate& c)
    {
        return (c.i == i)&&(c.j == j);
    }
};

class Widget : public QWidget
{
    Q_OBJECT

    QTableWidget* tableHorisontal;
    QTableWidget* tableVertical;
    QTableWidget* tableInput;
    QTableWidget* tableCosts;

    QList<Coordinate> *availableCoordList;
    QList<Coordinate> *UNavailableCoordList;
    QList<QList<Coordinate> *> *listOfPathes;

    QString resultStr;

    enum Direction {Vertical, Horisontal};
public:
    Widget(int rowCount=3, int columnCount=5, QWidget *parent = 0);
    ~Widget();
    bool havePathToRoot(int w, int h,
                        Coordinate* coord, Direction dir,
                        Coordinate root, QList<Coordinate> available, bool isFirstRun = false);

    QList<Coordinate> listHorisontaly(int w, Coordinate* coord, QList<Coordinate> available);
    QList<Coordinate> listVerticaly(int h, Coordinate* coord, QList<Coordinate> available);
    void findAvailablePositions();
    float calcCostOfAll();
    float calcCostOfPathApplying(const QList<Coordinate> *path);
    int applyPath(const QList<Coordinate> *path);

    void showPathForCell(int row, int col);
    float calcTriangleCost(QString str);
    QString calcTriangleResult();
    float calcPoint08(float x1, float x2, float y1, float y2);
public slots:
    void calcCostsValues();
    void onCalculateClicked();
    void formListOfPathes();
    void onCellClicked(int row, int col);
    int formBalanceForTheBestPath();

    void saveTablesToFile();
    void loadTablesFromFile();

    void calculateAll();
};

#endif // WIDGET_H
