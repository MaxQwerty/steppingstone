#-------------------------------------------------
#
# Project created by QtCreator 2015-05-13T13:36:13
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SteppingStone
TEMPLATE = app

CONFIG += c++11


SOURCES += main.cpp\
        widget.cpp \
    initialwidget.cpp

HEADERS  += widget.h \
    initialwidget.h
