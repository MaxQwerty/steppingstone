#include "widget.h"
#include <QSplitter>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonObject>
#include <QJsonArray>
#include <QFile>

Widget::Widget(int rowCount, int columnCount, QWidget *parent)
    : QWidget(parent)
{
    availableCoordList = new QList<Coordinate>;
    UNavailableCoordList = new QList<Coordinate>;
    listOfPathes = new QList<QList<Coordinate> *>;

    tableHorisontal = new QTableWidget(1,columnCount);
    tableHorisontal->setFixedHeight(50);
    tableHorisontal->verticalHeader()->hide();
    tableHorisontal->horizontalHeader()->hide();
    for(int i=0; i < tableHorisontal->columnCount(); ++i)
    {
        tableHorisontal->setItem(0,i, new QTableWidgetItem("0"));
        tableHorisontal->setColumnWidth(i, 70);
    }

    tableVertical = new QTableWidget(rowCount,1);
    tableVertical->verticalHeader()->hide();
    tableVertical->horizontalHeader()->hide();
    tableVertical->setMaximumWidth(75);
    tableVertical->setMinimumHeight(100);
    for(int i=0; i < tableVertical->rowCount(); ++i)
        tableVertical->setItem(i, 0, new QTableWidgetItem("0"));

    tableInput = new QTableWidget(rowCount,columnCount);
    tableInput->verticalHeader()->hide();
    tableInput->horizontalHeader()->hide();
    tableInput->setEditTriggers(QTableWidget::NoEditTriggers);
    tableInput->setSelectionMode(QAbstractItemView::NoSelection);
    tableInput->setMinimumHeight(100);
    for(int j=0; j < tableInput->columnCount(); ++j)
    {
        tableInput->setColumnWidth(j, 70);
    }

    tableCosts = new QTableWidget(rowCount, columnCount);
    tableCosts->verticalHeader()->hide();
    tableCosts->horizontalHeader()->hide();
    tableCosts->setMinimumHeight(100);
    for(int j=0; j < tableCosts->columnCount(); ++j)
    {
        tableCosts->setColumnWidth(j, 70);
    }

    for(int i=0; i < tableCosts->rowCount(); ++i)
        for(int j=0; j < tableCosts->columnCount(); ++j)
        {
            tableCosts->setItem(i,j, new QTableWidgetItem("0|0|0"));
        }

    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            tableInput->setItem(i,j, new QTableWidgetItem("0"));
        }

    QGridLayout* lay = new QGridLayout;

    QPushButton *btnCalcAll = new QPushButton("Вычислить");
    btnCalcAll->setFixedHeight(50);
    QPushButton *btnLoad = new QPushButton("Загрузить данные");
    QPushButton *btnSave = new QPushButton("Сохранить результат");

    QSplitter *splitter = new QSplitter(Qt::Vertical);
    splitter->addWidget(tableHorisontal);
    splitter->addWidget(tableInput);
    splitter->addWidget(tableCosts);

    lay->addWidget(btnCalcAll, 0, 0);
    lay->addWidget(splitter, 0, 1, 4, 1);
    lay->addWidget(tableVertical, 1, 0);
    lay->addWidget(btnLoad, 2, 0);
    lay->addWidget(btnSave, 3, 0);

    setLayout(lay);
    setMinimumSize(650, 400);
    setGeometry(300, 160, 0, 0);

    connect(btnCalcAll, SIGNAL(clicked()), SLOT(calculateAll()));
    connect(btnLoad, SIGNAL(clicked()), SLOT(loadTablesFromFile()));
    connect(btnSave, SIGNAL(clicked()), SLOT(saveTablesToFile()));

    connect(tableInput, SIGNAL(cellClicked(int,int)), SLOT(onCellClicked(int,int)));
}

Widget::~Widget()
{

}

void Widget::onCalculateClicked()
{
    setEnabled(false);

    findAvailablePositions();

    setEnabled(true);
}

void Widget::findAvailablePositions()
{
    availableCoordList->clear();
    UNavailableCoordList->clear();
    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            if(tableInput->item(i,j)->text().toInt() != 0)
                availableCoordList->append(Coordinate(i, j));
            else
                UNavailableCoordList->append(Coordinate(i, j));
        }
}

float Widget::calcCostOfAll()
{
    float COST = 0.0;
    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            COST += tableInput->item(i,j)->text().toInt() *
                    calcTriangleCost(tableCosts->item(i,j)->text());
        }
    return COST;
}

float Widget::calcCostOfPathApplying(const QList<Coordinate> *path)
{
    bool mustBePlus = true;
    QList<Coordinate>::const_iterator it = path->begin();
    for(; it != path->end(); ++it)
    {
        int curVal = tableInput->item(it->i, it->j)->text().toInt();
        curVal += (mustBePlus ? 1 : -1);
        tableInput->item(it->i, it->j)->setText(QString::number(curVal));
        mustBePlus = !mustBePlus;
    }
    float resPathCost = calcCostOfAll();

    mustBePlus = false;
    it = path->begin();
    for(; it != path->end(); ++it)
    {
        int curVal = tableInput->item(it->i, it->j)->text().toInt();
        curVal += (mustBePlus ? 1 : -1);
        tableInput->item(it->i, it->j)->setText(QString::number(curVal));
        mustBePlus = !mustBePlus;
    }
    return resPathCost;
}

int Widget::applyPath(const QList<Coordinate> *path)
{
    int minimumVal = tableInput->item(path->at(1).i, path->at(1).j)->text().toInt();
    for(int i=1; i < path->length(); i += 2)
    {
        int curVal = tableInput->item(path->at(i).i, path->at(i).j)->text().toInt();
        if(curVal < minimumVal)
            minimumVal = curVal;
    }


    bool mustBePlus = true;
    QList<Coordinate>::const_iterator it = path->begin();
    for(; it != path->end(); ++it)
    {
        int curVal = tableInput->item(it->i, it->j)->text().toInt();
        curVal += (mustBePlus ? minimumVal : -minimumVal);
        tableInput->item(it->i, it->j)->setText(QString::number(curVal));
        mustBePlus = !mustBePlus;
    }
    return calcCostOfAll();
}

int Widget::formBalanceForTheBestPath()
{
    float bestPathVal = calcCostOfAll();
    int bestPathIndx = -1;

    for(int i=0; i < listOfPathes->length(); ++i)
    {
        float curPathVal = calcCostOfPathApplying(listOfPathes->at(i));
        if(curPathVal < bestPathVal)
        {
            bestPathIndx = i;
            bestPathVal = curPathVal;
        }
    }
    if(bestPathIndx != -1)
    {
        return applyPath(listOfPathes->at(bestPathIndx));
    }
    else
        return -1;
}

void Widget::formListOfPathes()
{
    listOfPathes->clear();
    QList<Coordinate>::iterator it = UNavailableCoordList->begin();
    for(; it != UNavailableCoordList->end(); ++it)
    {
        Coordinate check(it->i, it->j);
        availableCoordList->append(check);
        bool isValid = havePathToRoot(tableInput->columnCount(), tableInput->rowCount(), &check, Horisontal, check, *availableCoordList, true);
        if(isValid)
        {
            QList<Coordinate> *curPathList = new QList<Coordinate>;
            Coordinate* firstPos = &check;
            while (true)
            {
                if(firstPos->nextCoordinates.length() == 0)
                    break;
                curPathList->append(Coordinate(firstPos->i, firstPos->j));
                firstPos = firstPos->nextCoordinates.at(0);
            }
            listOfPathes->append(curPathList);
        }
        availableCoordList->removeOne(check);
    }
}

void Widget::showPathForCell(int row, int col)
{
    Coordinate check(row, col);
    availableCoordList->append(check);
    havePathToRoot(tableInput->columnCount(), tableInput->rowCount(), &check, Horisontal, check, *availableCoordList, true);
    Coordinate* firstPos = &check;
    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
            tableInput->item(i, j)->setBackground(Qt::transparent);
    while (true)
    {
        if(firstPos->nextCoordinates.length() == 0)
            break;
        tableInput->item(firstPos->i, firstPos->j)->setBackground(Qt::yellow);
        firstPos = firstPos->nextCoordinates.at(0);
    }
    availableCoordList->removeOne(check);
}

float Widget::calcTriangleCost(QString str)
{
    int a, b, c;
    QStringList triList = str.split("|");
    a = triList.at(0).toInt();
    b = triList.at(1).toInt();
    c = triList.at(2).toInt();

    return (a + 2*b + c) / 2;
}

QString Widget::calcTriangleResult()
{
    float a=0.0, b=0.0, c=0.0;
    int curVal=0;
    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            curVal = tableInput->item(i, j)->text().toInt();
            QStringList triList = tableCosts->item(i,j)->text().split("|");
            a += triList.at(0).toInt() * curVal;
            b += triList.at(1).toInt() * curVal;
            c += triList.at(2).toInt() * curVal;
        }
    QString outStr = QString("(%1, %2, %3)").arg(a).arg(b).arg(c);
    return outStr;
}

float Widget::calcPoint08(float x1, float x2, float y1, float y2)
{
    float a = (y2 - y1) / (x2 - x1);
    float b = y2 - a * x2;
    return (0.8 - b) / a;
}

void Widget::saveTablesToFile()
{
    QJsonArray arrHorisontal;
    for(int i=0; i < tableHorisontal->columnCount(); ++i)
    {
        arrHorisontal.append(QJsonValue(tableHorisontal->item(0, i)->text()));
    }

    QJsonArray arrVertical;
    for(int i=0; i < tableVertical->rowCount(); ++i)
    {
        arrVertical.append(QJsonValue(tableVertical->item(i, 0)->text()));
    }

    QJsonArray arrVolume;
    for(int i=0; i < tableInput->rowCount(); ++i)
    {
        QJsonArray arrRowVolume;
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            arrRowVolume.append(QJsonValue(tableInput->item(i,j)->text()));
        }
        arrVolume.append(QJsonValue(arrRowVolume));
    }

    QJsonArray arrCost;
    for(int i=0; i < tableCosts->rowCount(); ++i)
    {
        QJsonArray arrRowCost;
        for(int j=0; j < tableCosts->columnCount(); ++j)
        {
            arrRowCost.append(QJsonValue(tableCosts->item(i,j)->text()));
        }
        arrCost.append(QJsonValue(arrRowCost));
    }

    QJsonObject obj;
    obj.insert("Склады", QJsonValue(arrVertical));
    obj.insert("Потребители(магазины)", QJsonValue(arrHorisontal));
    obj.insert("Оптимальный план перевозок", QJsonValue(arrVolume));
    obj.insert("Транспортные расходы", QJsonValue(arrCost));

    QJsonDocument doc;
    doc.setObject(obj);

    QFile file("Результат.txt");
    file.open(QIODevice::WriteOnly);
    file.write(doc.toJson().append(resultStr));
    file.close();
}

void Widget::loadTablesFromFile()
{
    QFile file("Входные данные.txt");
    file.open(QIODevice::ReadOnly);
    QJsonDocument doc = QJsonDocument::fromJson(file.readAll());
    file.close();

    QJsonObject obj = doc.object();
    QJsonArray arrVertical = obj.value("Склады").toArray();
    QJsonArray arrHorisontal = obj.value("Потребители(магазины)").toArray();
    QJsonArray arrCost = obj.value("Транспортные расходы").toArray();

    for(int i=0; i < tableHorisontal->columnCount(); ++i)
    {
        tableHorisontal->item(0, i)->setText(arrHorisontal.at(i).toString());
    }

    for(int i=0; i < tableVertical->rowCount(); ++i)
    {
        tableVertical->item(i, 0)->setText(arrVertical.at(i).toString());
    }

    for(int i=0; i < tableCosts->rowCount(); ++i)
    {
        QJsonArray arrRowCost = arrCost.at(i).toArray();
        for(int j=0; j < tableCosts->columnCount(); ++j)
        {
            tableCosts->item(i, j)->setText(arrRowCost.at(j).toString());
        }
    }
}

void Widget::calcCostsValues()
{
    for(int i=0; i < tableInput->rowCount(); ++i)
        for(int j=0; j < tableInput->columnCount(); ++j)
        {
            tableInput->setItem(i,j, new QTableWidgetItem("0"));
        }
    int i=0, j=0;

    int k=0;
    int *horArr = new int[tableHorisontal->columnCount()];
    int horSum = 0;
    for(; k < tableHorisontal->columnCount(); ++k)
    {
        int val = tableHorisontal->item(0, k)->text().toInt();
        horArr[k] = val;
        horSum += val;
    }

    k=0;
    int *verArr = new int[tableVertical->rowCount()];
    int verSum = 0;
    for(; k < tableVertical->rowCount(); ++k)
    {
        int val = tableVertical->item(k, 0)->text().toInt();
        verArr[k] = val;
        verSum += val;
    }

    if(horSum != verSum)
    {
        QMessageBox errMsg;
        errMsg.setText("Количества товаров не совпадают!");
        errMsg.setIcon(QMessageBox::Critical);
        errMsg.exec();
        return;
    }

    bool mustMoveHorisontaly = false;
    int curVal = 0;
    while(true)
    {
        mustMoveHorisontaly = verArr[i] > horArr[j];
        curVal = mustMoveHorisontaly ? horArr[j] : verArr[i];
        tableInput->item(i, j)->setText(QString::number(curVal));
        horArr[j] -= curVal;
        verArr[i] -= curVal;
        if(mustMoveHorisontaly)
            ++j;
        else
            ++i;

        if((j >= tableHorisontal->columnCount()) || (i >= tableVertical->rowCount()))
            break;
    }
    delete [] verArr;
    delete [] horArr;
}

void Widget::onCellClicked(int row, int col)
{
    if(availableCoordList->length() > 0)
        showPathForCell(row, col);
}

void Widget::calculateAll()
{
    calcCostsValues();
    int itersCount = 0;
    QString startCost = calcTriangleResult();
    while(true)
    {
        ++itersCount;
        findAvailablePositions();
        formListOfPathes();
        if(formBalanceForTheBestPath() == -1)
            break;
    }
    QString resCost = calcTriangleResult();

    QStringList resTriList = resCost.remove("(").remove(")").split(", ");
    float leftRes08 = calcPoint08(resTriList.at(0).toFloat(), resTriList.at(1).toFloat(), 0, 1);
    float rightRes08 = calcPoint08(resTriList.at(1).toFloat(), resTriList.at(2).toFloat(), 1, 0);

    resultStr = QString("Начальные общие транспортные расходы: %1\n"
                          "Конечные общие транспортные расходы: %2\n"
                          "Количество итераций: %3\n"
                          "Значения при 0.8: %4, %5\n").arg(startCost).arg(resCost).arg(itersCount).arg(leftRes08).arg(rightRes08);
    QMessageBox infoMsg;
    infoMsg.setText(resultStr);
    infoMsg.setIcon(QMessageBox::Information);
    infoMsg.exec();
}

bool Widget::havePathToRoot(int w, int h,
                            Coordinate* coord, Widget::Direction dir,
                            Coordinate root, QList<Coordinate> available, bool isFirstRun)
{
    if(dir == Horisontal)
    {
        if((*coord == root) && (!isFirstRun))
            return true;
        QList<Coordinate> candidates = listHorisontaly(w, coord, available);
        if(candidates.length() == 0)
            return false;
        bool isPartOfPathToRoot = false;
        for(QList<Coordinate>::iterator it = candidates.begin(); it != candidates.end(); ++it)
        {
            bool curHavePathToRoot = false;
            available.removeOne(*it);
            Coordinate* currentNode = new Coordinate(it->i, it->j);
            curHavePathToRoot = havePathToRoot(w, h, currentNode, Vertical, root, available);
            if(curHavePathToRoot)
            {
                coord->nextCoordinates.append(currentNode);
            }
            else
            {
                delete currentNode;
            }
            isPartOfPathToRoot = isPartOfPathToRoot || curHavePathToRoot;
            available.append(*it);
        }
        return isPartOfPathToRoot;
    }
    else
    {
        QList<Coordinate> candidates = listVerticaly(h, coord, available);
        if(candidates.length() == 0)
            return false;
        bool isPartOfPathToRoot = false;
        for(QList<Coordinate>::iterator it = candidates.begin(); it != candidates.end(); ++it)
        {
            bool curHavePathToRoot = false;
            available.removeOne(*it);
            Coordinate* currentNode = new Coordinate(it->i, it->j);
            curHavePathToRoot = havePathToRoot(w, h, currentNode, Horisontal, root, available);
            if(curHavePathToRoot)
            {
                coord->nextCoordinates.append(currentNode);
            }
            else
            {
                delete currentNode;
            }
            isPartOfPathToRoot = isPartOfPathToRoot || curHavePathToRoot;
            available.append(*it);
        }
        return isPartOfPathToRoot;
    }
}

QList<Coordinate> Widget::listHorisontaly(int w, Coordinate *coord, QList<Coordinate> availables)
{
    QList<Coordinate> horList;
    for(int j=0; j < w; ++j)
    {
        Coordinate cor(coord->i,j);
        if(availables.contains(cor) && !(*coord == cor))
            horList.append(cor);
    }
    return horList;
}

QList<Coordinate> Widget::listVerticaly(int h, Coordinate *coord, QList<Coordinate> availables)
{
    QList<Coordinate> verList;
    for(int i=0; i < h; ++i)
    {
        Coordinate cor(i, coord->j);
        if(availables.contains(cor) && !(*coord == cor))
            verList.append(cor);
    }
    return verList;
}
