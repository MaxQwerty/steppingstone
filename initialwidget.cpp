#include "initialwidget.h"

InitialWidget::InitialWidget(QWidget *parent) : QWidget(parent)
{
    lay = new QVBoxLayout();

    spinRowCount = new QSpinBox();
    spinRowCount->setValue(3);
    spinColumnCount = new QSpinBox();
    spinColumnCount->setValue(5);
    btnCreateCalcWidget = new QPushButton("Сформировать поле ввода");
    calcWidget = 0;

    lay->addWidget(spinRowCount);
    lay->addWidget(spinColumnCount);
    lay->addWidget(btnCreateCalcWidget);
    setLayout(lay);

    connect(btnCreateCalcWidget, SIGNAL(clicked()), SLOT(createdAndShowCalcWidget()));
}

InitialWidget::~InitialWidget()
{

}

void InitialWidget::createdAndShowCalcWidget()
{
    if(calcWidget)
    {
        lay->removeWidget(calcWidget);
        delete calcWidget;
    }
    calcWidget = new Widget(spinRowCount->value(), spinColumnCount->value());
    lay->addWidget(calcWidget);
}
