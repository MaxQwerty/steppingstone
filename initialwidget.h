#ifndef INITIALWIDGET_H
#define INITIALWIDGET_H

#include <QWidget>
#include "widget.h"
#include <QPushButton>
#include <QSpinBox>
#include <QLayout>

class InitialWidget : public QWidget
{
    Q_OBJECT
    Widget *calcWidget;
    QSpinBox *spinRowCount;
    QSpinBox *spinColumnCount;
    QPushButton *btnCreateCalcWidget;
    QVBoxLayout *lay;
public:
    explicit InitialWidget(QWidget *parent = 0);
    ~InitialWidget();

signals:

public slots:
    void createdAndShowCalcWidget();
};

#endif // INITIALWIDGET_H
